﻿using System.Reflection;
using System.Windows;

namespace FWLibUI.Views
{
    public partial class MainView : Window
    {
        public MainView()
        {
            InitializeComponent();

           Title += " v" + Assembly.GetEntryAssembly().GetName().Version.ToString(3);
        }


        private void ClearButton_OnClick(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
