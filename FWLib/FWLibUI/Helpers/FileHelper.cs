﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Windows.Forms;
using CSharpFunctionalExtensions;
using FWLibUI.Domain.Entities;

namespace FWLibUI.Helpers
{
    public static class FileHelper
    {
        /// <summary>
        /// Copy file to path
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <param name="targetPath"></param>
        /// <returns></returns>
        public static Result CopyFile(string sourcePath, string targetPath)
        {
            try
            {
                File.Copy(sourcePath, targetPath);
                return Result.Ok();
            }
            catch (Exception e)
            {
                return Result.Fail(e.Message);
            }
        }

        /// <summary>
        /// VAlidate FWFile by rules
        /// </summary>
        /// <param name="fwFile"></param>
        /// <returns></returns>
        public static Result ValidateFile(FWFile fwFile)
        {
            if ( fwFile.MD5 == null
                || string.IsNullOrWhiteSpace(fwFile.FileType)
                || string.IsNullOrWhiteSpace(fwFile.CarBrand)
                || string.IsNullOrWhiteSpace(fwFile.CarModel)
                || string.IsNullOrWhiteSpace(fwFile.ControlUnitType)
                || string.IsNullOrWhiteSpace(fwFile.ControllerBrand)
                || string.IsNullOrWhiteSpace(fwFile.ControllerModel)
                || string.IsNullOrWhiteSpace(fwFile.MemoryType))
            {
                return Result.Fail("Some necessarily fileld(s) is empty");
            }

            return Result.Ok();
        }

        /// <summary>
        /// Campare MD5 hash of two files 
        /// </summary>
        /// <param name="file1"></param>
        /// <param name="file2"></param>
        /// <returns></returns>
        public static Result CompareFilesHash(byte[] file1, byte[] file2)
        {
            var hashFile1 = MD5.Create().ComputeHash(file1);
            var hashFile2 = MD5.Create().ComputeHash(file2);

            if (hashFile1.SequenceEqual(hashFile2))
            {
                return Result.Ok();
            }
            
            return Result.Fail("Hash sum is not equl.");
        }

        /// <summary>
        /// Make unic file name for save is in database
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string MakeUnicFileName(string fileName)
        {
            var clearFileName = Guid.NewGuid() + "-" + Path.GetFileName(fileName);
            return clearFileName;
        }

        /// <summary>
        /// Save file dialog
        /// </summary>
        /// <returns></returns>
        public static string FileSaveDialog(string fileName = "newfile.bin")
        {
            string fwFilePath = string.Empty;
            var fd = new System.Windows.Forms.SaveFileDialog();

            fd.InitialDirectory = @"C:\";
            fd.Title = @"Save binary files";
            fd.CheckFileExists = false;
            fd.CheckPathExists = true;
            fd.DefaultExt = "txt";
            fd.Filter = @"Binary file |*.bin|All files |*.*";
            fd.FilterIndex = 1;
            fd.RestoreDirectory = true;
            fd.FileName = fileName;

            if (fd.ShowDialog() == DialogResult.OK)
            {
                fwFilePath = fd.FileName;
            }

            return fwFilePath;
        }
        
        /// <summary>
        /// Save iamge dialog
        /// </summary>
        /// <returns></returns>
        public static string ImageSaveDialog(string fileName = "newfile.jpg")
        {
            string fwFilePath = string.Empty;
            var fd = new System.Windows.Forms.SaveFileDialog();

            fd.InitialDirectory = @"C:\";
            fd.Title = @"Save binary files";
            fd.CheckFileExists = false;
            fd.CheckPathExists = true;
            fd.DefaultExt = "jpg";
            fd.Filter = @"Binary file |*.jpg|All files |*.*";
            fd.FilterIndex = 1;
            fd.RestoreDirectory = true;
            fd.FileName = fileName;

            if (fd.ShowDialog() == DialogResult.OK)
            {
                fwFilePath = fd.FileName;
            }

            return fwFilePath;
        }


        /// <summary>
        /// Open image dialog
        /// </summary>
        /// <returns></returns>
        public static string ImageOpenDialog()
        {
            string fwFilePath = string.Empty;
            var fd = new System.Windows.Forms.OpenFileDialog();

            fd.InitialDirectory = @"C:\";
            fd.Title = @"Save image files";
            fd.CheckFileExists = false;
            fd.CheckPathExists = true;
            fd.DefaultExt = "jpg";
            fd.Filter = @"Binary file |*.jpg|All files |*.*";
            fd.FilterIndex = 1;
            fd.RestoreDirectory = true;

            if (fd.ShowDialog() == DialogResult.OK)
            {
                fwFilePath = fd.FileName;
            }

            return fwFilePath;
        }
        
        /// <summary>
        /// Save database dialog
        /// </summary>
        /// <returns></returns>
        public static string DatabaseSaveDialog(string fileName = "newdatabase.db")
        {
            string fwFilePath = string.Empty;
            var fd = new System.Windows.Forms.SaveFileDialog();

            fd.InitialDirectory = @"C:\";
            fd.Title = @"Save binary files";
            fd.CheckFileExists = false;
            fd.CheckPathExists = true;
            fd.DefaultExt = "txt";
            fd.Filter = @"Binary file |*.db|All files |*.*";
            fd.FilterIndex = 1;
            fd.RestoreDirectory = true;
            fd.FileName = fileName;

            if (fd.ShowDialog() == DialogResult.OK)
            {
                fwFilePath = fd.FileName;
            }

            return fwFilePath;
        }
    }
}