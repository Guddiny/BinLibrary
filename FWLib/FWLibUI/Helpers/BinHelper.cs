﻿using System.Text;

namespace FWLibUI.Helpers
{
    public static class BinHelper
    {
        public static string BinToString(this byte[] data)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sb.Append(data[i].ToString("X2").ToLower());
            }

            return sb.ToString();
        }
    }
}