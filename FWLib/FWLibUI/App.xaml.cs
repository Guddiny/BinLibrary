﻿using System.Windows.Threading;
using NLog;

namespace FWLibUI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private Logger _log = NLog.LogManager.GetCurrentClassLogger();

        private void App_OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
#if !DEBUG
                _log.Fatal(e.Exception);
#endif
        }
    }
}