﻿using CSharpFunctionalExtensions;
using FWLibUI.Domain.Abstract;
using System.Collections.Generic;
using System.Linq;
using FWLibUI.Domain.Concrete;

namespace FWLibUI.Domain.Services
{
    public class ControlUnitService
    {
        private readonly IFWFileRepository _fileRepository;


        public ControlUnitService()
        {
            _fileRepository = new FWFileRepository();
        }

        public Result<List<string>> GetAllControlUnits()
        {
            var result = _fileRepository.FWFiles;

            if (result.IsFailure)
            {
                return Result.Fail<List<string>>(result.Error);
            }

            return Result.Ok(result.Value.GroupBy(c => c.ControlUnit).Select(k => k.Key).ToList());
        }

        public Result<List<string>> GetAllControlUnitTypes()
        {
            var result = _fileRepository.FWFiles;

            if (result.IsFailure)
            {
                return Result.Fail<List<string>>(result.Error);
            }

            return Result.Ok(result.Value.GroupBy(t => t.ControlUnitType).Select(k => k.Key).ToList());
        }

        public Result<List<string>> GetAllControlUnitNumbers()
        {
            var result = _fileRepository.FWFiles;

            if (result.IsFailure)
            {
                return Result.Fail<List<string>>(result.Error);
            }

            return Result.Ok(result.Value.GroupBy(t => t.ControlUnitNumber).Select(k => k.Key).ToList());
        }
    }
}