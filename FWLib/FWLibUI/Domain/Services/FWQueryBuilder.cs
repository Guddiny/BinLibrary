﻿using System;
using System.Globalization;
using System.Linq.Expressions;
using CSharpFunctionalExtensions;
using FWLibUI.Domain.Entities;
using DynamicExpression = System.Linq.Dynamic.DynamicExpression;

namespace FWLibUI.Domain.Services
{
    public class FWQueryBuilder
    {
        public string BuildQueryString(string fileType, string carBrand, string carModel, string engineType,
            decimal engineVolume, string transmissionType, string controlUnitType, string controlUnit, string ecuNumber,
            string controllerBrand, string controllerModel, string memoryType, string vin)
        {
            string query = string.Empty;

            if (!string.IsNullOrWhiteSpace(fileType))
            {
                query += ((!string.IsNullOrWhiteSpace(query) ? " AND " : "") + $"FileType = \"{fileType}\"");
            }

            if (!string.IsNullOrWhiteSpace(carBrand))
            {
                query += ((!string.IsNullOrWhiteSpace(query) ? " AND " : "") + $"CarBrand = \"{carBrand}\"");
            }

            if (!string.IsNullOrWhiteSpace(carModel))
            {
                query += ((!string.IsNullOrWhiteSpace(query) ? " AND " : "") + $"CarModel = \"{carModel}\"");
            }

            if (!string.IsNullOrWhiteSpace(engineType))
            {
                query += ((!string.IsNullOrWhiteSpace(query) ? " AND " : "") + $"EngineType = \"{engineType}\"");
            }

            if (engineVolume != 0)
            {
                query += ((!string.IsNullOrWhiteSpace(query) ? " AND " : "") +
                           $"EngineVolume = {engineVolume}");
            }
            
            if (!string.IsNullOrWhiteSpace(transmissionType))
            {
                query += ((!string.IsNullOrWhiteSpace(query) ? " AND " : "") + $"TransmissionType = \"{transmissionType}\"");
            }
            
            if (!string.IsNullOrWhiteSpace(controlUnitType))
            {
                query += ((!string.IsNullOrWhiteSpace(query) ? " AND " : "") + $"ControlUnitType = \"{controlUnitType}\"");
            }
            
            if (!string.IsNullOrWhiteSpace(controlUnit))
            {
                query += ((!string.IsNullOrWhiteSpace(query) ? " AND " : "") + $"ControlUnit = \"{controlUnit}\"");
            }
            
            if (!string.IsNullOrWhiteSpace(ecuNumber))
            {
                query += ((!string.IsNullOrWhiteSpace(query) ? " AND " : "") + $"ControlUnitNumber = \"{ecuNumber}\"");
            }
            
            if (!string.IsNullOrWhiteSpace(controllerBrand))
            {
                query += ((!string.IsNullOrWhiteSpace(query) ? " AND " : "") + $"ControllerBrand = \"{controllerBrand}\"");
            }
            
            if (!string.IsNullOrWhiteSpace(controllerModel))
            {
                query += ((!string.IsNullOrWhiteSpace(query) ? " AND " : "") + $"ControllerModel = \"{controllerModel}\"");
            }
            
            if (!string.IsNullOrWhiteSpace(memoryType))
            {
                query += ((!string.IsNullOrWhiteSpace(query) ? " AND " : "") + $"MemoryType = \"{memoryType}\"");
            }
            
            if (!string.IsNullOrWhiteSpace(vin))
            {
                query += ((!string.IsNullOrWhiteSpace(query) ? " AND " : "") + $"VIN = \"{vin}\"");
            }

            return query;
        }

        public Result<Expression<Func<FWFile, bool>>> GenerateQueryExpression(string queryString)
        {
            Expression<Func<FWFile, bool>> parssedExpression = null;
            if (!string.IsNullOrWhiteSpace(queryString))
            {
                parssedExpression =
                    (Expression<Func<FWFile, bool>>)DynamicExpression.ParseLambda(typeof(FWFile),
                        typeof(bool), queryString);

                return Result.Ok<Expression<Func<FWFile, bool>>>(parssedExpression);
            }

            return Result.Fail<Expression<Func<FWFile, bool>>>("Inavlid query string");
        }
    }
}