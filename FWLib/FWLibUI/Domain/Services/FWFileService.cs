﻿using CSharpFunctionalExtensions;
using FWLibUI.Domain.Abstract;
using FWLibUI.Domain.Concrete;
using FWLibUI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FWLibUI.Domain.Services
{
    public class FWFileService
    {
        private IFWFileRepository _fileRepository;

        public FWFileService()
        {
            _fileRepository = new FWFileRepository();
        }

        /// <summary>
        /// For unit tests
        /// </summary>
        /// <param name="fileRepository"></param>
        public void FWFileServiceSetPepository(IFWFileRepository fileRepository)
        {
            _fileRepository = fileRepository;
        }

        public Result<List<FWFile>> GetAllFiles()
        {
            var result = _fileRepository.FWFiles;
            if (result.IsFailure)
            {
                return Result.Fail<List<FWFile>>(result.Error);
            }

            return Result.Ok(result.Value.ToList());
        }


        public Result<List<FWFile>> GetAllFiles(int skip, int take)
        {
            var result = _fileRepository.FWFiles;

            if (result.IsFailure)
            {
                return Result.Fail<List<FWFile>>(result.Error);
            }

            return Result.Ok(result.Value.Skip(skip).Take(take).ToList());
        }

        public Result<List<string>> GetAllFileTypes()
        {
            var result = _fileRepository.FWFiles;
            if (result.IsFailure)
            {
                return Result.Fail<List<string>>(result.Error);
            }

            return Result.Ok(result.Value.GroupBy(t => t.FileType).Select(k => k.Key).ToList());
        }

        public Result<List<String>> GetAllVIN()
        {
            var result = _fileRepository.FWFiles.Value
                .GroupBy(p => p.VIN)
                .Select(k => k.Key)
                .ToList();

            return Result.Ok(result);
        }

        public Result<List<string>> GetAllMemoryTypes()
        {
            var result = _fileRepository.FWFiles;

            if (result.IsFailure)
            {
                return Result.Fail<List<string>>(result.Error);
            }

            return Result.Ok(result.Value.GroupBy(m => m.MemoryType).Select(k => k.Key).ToList());
        }

        public Result<List<string>> GetAllECUNumbers()
        {
            var result = _fileRepository.FWFiles.Value
                .GroupBy(n => n.ControlUnitNumber)
                .Select(k => k.Key)
                .ToList();

            return Result.Ok(result);
        }

        public Result<int> GetTotalItemsCount()
        {
            var result = _fileRepository.Count(p => p.Id != 0);

            if (result.IsFailure)
            {
                return Result.Fail<int>(result.Error);
            }

            return Result.Ok(result.Value);
        }

        public Result SaveNewFile(FWFile fWFile)
        {
            var saveResult = _fileRepository.Add(fWFile);

            if (saveResult.IsSuccess)
            {
                return Result.Ok();
            }

            return Result.Fail(saveResult.Error);
        }

        public Result UpdatFile(FWFile file)
        {
            var updateResult = _fileRepository.FWFiles
                .OnSuccess(f => _fileRepository.Update(file));

            if (updateResult.IsFailure)
                return Result.Fail<FWFile>(updateResult.Error);

            return Result.Ok();
        }
        
        public Result DeleteFile(FWFile file)
        {
            var deleteResult = _fileRepository.Delete(file);

            if (deleteResult.IsFailure)
                return Result.Fail<FWFile>(deleteResult.Error);

            return Result.Ok();
        }

        public Result<List<FWFile>> FindFiles(Expression<Func<FWFile, bool>> predicate)
        {
            var findresult = _fileRepository.Find(predicate);

            if (findresult.IsFailure)
            {
                return Result.Fail<List<FWFile>>(findresult.Error);
            }

            return Result.Ok<List<FWFile>>(findresult.Value.ToList());
        }

    }
}