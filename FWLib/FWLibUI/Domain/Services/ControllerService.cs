﻿using CSharpFunctionalExtensions;
using FWLibUI.Domain.Abstract;
using System.Collections.Generic;
using System.Linq;
using FWLibUI.Domain.Concrete;

namespace FWLibUI.Domain.Services
{
    public class ControllerService
    {
        private readonly IFWFileRepository _fileRepository;

        public ControllerService()
        {
            _fileRepository = new FWFileRepository();
        }

        public Result<List<string>> GetAllControllerBrands()
        {
            var result = _fileRepository.FWFiles;

            if (result.IsFailure)
            {
                return Result.Fail<List<string>>(result.Error);
            }

            return Result.Ok(result.Value.GroupBy(t => t.ControllerBrand).Select(k => k.Key).ToList());
        }

        public Result<List<string>> GetAllControllerModels()
        {
            var result = _fileRepository.FWFiles;

            if (result.IsFailure)
            {
                return Result.Fail<List<string>>(result.Error);
            }

            return Result.Ok(result.Value.GroupBy(t => t.ControllerModel).Select(k => k.Key).ToList());
        }
    }
}