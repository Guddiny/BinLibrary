﻿using System.Collections.Generic;
using System.Linq;
using CSharpFunctionalExtensions;
using FWLibUI.Domain.Abstract;
using FWLibUI.Domain.Entities;

namespace FWLibUI.Domain.Services
{
    public class ImageService
    {
        private readonly IImageRepository _imageRepository;

        public ImageService(IImageRepository imageRepository)
        {
            _imageRepository = imageRepository;
        }

        public Result<List<Image>> GetImageByUnit(string name, string number)
        {
            var findResult = _imageRepository.Find(i => i.ControlUnitName == name && i.ControlUnitNumber == number);

            if (findResult.IsFailure)
            {
                return Result.Fail<List<Image>>(findResult.Error);
            }
            
            return Result.Ok(findResult.Value.ToList());
        }


        public Result SaveImage(Image image)
        {
            var saveResult = _imageRepository.Add(image);

            if (saveResult.IsFailure)
            {
                return Result.Fail(saveResult.Error);
            }
            
            return Result.Ok();
        }

        public Result DeleteImage(Image image)
        {
            var deleteResult = _imageRepository.Delete(image);

            if (deleteResult.IsFailure)
            {
                return Result.Fail(deleteResult.Error);
            }
            
            return Result.Ok();
        }
    }
}