﻿using System;
using System.IO;
using CSharpFunctionalExtensions;
using FWLibUI.Domain.Entities;
using Newtonsoft.Json;

namespace FWLibUI.Domain.Services
{
    public class AppConfigService
    {
        public readonly string DefaultAppConfigPath = AppDomain.CurrentDomain.BaseDirectory;
        public const string DefaultConfigFileName = "binlibcfg.json";
        private const string TempFileFolderNmae = @"Temp";
        public readonly string TempFileFolderPath = string.Empty;

        public AppConfigService()
        {
            DefaultAppConfigPath = AppDomain.CurrentDomain.BaseDirectory;
            TempFileFolderPath = DefaultAppConfigPath + "/" + TempFileFolderNmae;
        }

        public Result<AppConfig> ReadAppConfig(string appConfigPath,
            string fileName = DefaultConfigFileName)
        {
            if (File.Exists(appConfigPath + fileName))
            {
                AppConfig cfg = null;
                
                using (StreamReader file = File.OpenText(appConfigPath + fileName))
                {
                    JsonReader reader = new JsonTextReader(file);
                    var serialiser = new JsonSerializer
                    {
                        Formatting = Formatting.Indented
                    };
                    try
                    {
                        cfg = serialiser.Deserialize<AppConfig>(reader);
                    }
                    catch (Exception e)
                    {
                        return Result.Fail<AppConfig>(e.Message);
                    }
                }

                return Result.Ok(cfg);
            }
            else
            {
                return Result.Fail<AppConfig>("File not found");
            }
        }

        public Result SaveAppConfig(AppConfig appConfig, string appConfigPath,
            string fileName = DefaultConfigFileName)
        {
            try
            {
                using (StreamWriter file = File.CreateText(DefaultAppConfigPath + fileName))
                {
                    var serialiser = new JsonSerializer
                    {
                        Formatting = Formatting.Indented
                    };

                    serialiser.Serialize(file, appConfig);
                }

                return Result.Ok();
            }
            catch (Exception e)
            {
                return Result.Fail(e.Message);
            }
        }
    }
}