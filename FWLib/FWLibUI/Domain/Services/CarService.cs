﻿using CSharpFunctionalExtensions;
using FWLibUI.Domain.Abstract;
using FWLibUI.Domain.Concrete;
using FWLibUI.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace FWLibUI.Domain.Services
{
    public class CarService
    {
        private readonly IFWFileRepository _fileRepository;


        public CarService()
        {
            _fileRepository = new FWFileRepository();
        }

        public Result<List<string>> GetAllCarBrands()
        {
            var result = _fileRepository.FWFiles;

            if (result.IsFailure)
            {
                return Result.Fail<List<string>>(result.Error);
            }

            var r = from file in result.Value
                    group file by file.CarBrand into g
                    select (g.Key.ToList());

            return Result.Ok(result.Value.GroupBy(t => t.CarBrand).Select(k => k.Key).ToList());
        }

        public Result<List<string>> GetAllEngineTypes()
        {
            var result = _fileRepository.FWFiles;

            if (result.IsFailure)
            {
                return Result.Fail<List<string>>(result.Error);
            }

            return Result.Ok(result.Value.GroupBy(t => t.EngineType).Select(k => k.Key).ToList());
        }

        public Result<List<string>> GetAllTransmissionTypes()
        {
            var result = _fileRepository.FWFiles;

            if (result.IsFailure)
            {
                return Result.Fail<List<string>>(result.Error);
            }

            return Result.Ok(result.Value.GroupBy(t => t.TransmissionType).Select(k => k.Key).ToList());
        }

        public Result<List<string>> GetRelatedModels(string carBrand)
        {
            var result = _fileRepository.Find(p => p.CarBrand == carBrand);

            if (result.IsFailure)
            {
                return Result.Fail<List<string>>(result.Error);
            }

            if (carBrand == null)
            {
                return Result.Fail<List<string>>("");
            }

            return Result.Ok(result.Value.GroupBy(b => b.CarModel).Select(k => k.Key).ToList());
        }
    }
}