﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using CSharpFunctionalExtensions;
using FWLibUI.Domain.Entities;

namespace FWLibUI.Domain.Abstract
{
    public interface IFWFileRepository
    {
        Result<IEnumerable<FWFile>> FWFiles { get; }
        Result Add(FWFile fwFile);
        Result Delete(FWFile fwFile);
        Result Update(FWFile fwFile);
        Result<int> Count(Expression<Func<FWFile, bool>> predicate);
        Result<IEnumerable<FWFile>> Find(Expression<Func<FWFile, bool>> predicate);
    }
}