﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using CSharpFunctionalExtensions;
using FWLibUI.Domain.Entities;

namespace FWLibUI.Domain.Abstract
{
    public interface IImageRepository
    {
        Result<IEnumerable<Image>> Images { get; }
        Result Delete(Image image);
        Result Add(Image image);
        Result Update(Image image);
        Result<IEnumerable<Image>> Find(Expression<Func<Image, bool>> predicate);

    }
}