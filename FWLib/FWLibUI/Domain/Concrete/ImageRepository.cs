﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using CSharpFunctionalExtensions;
using FWLibUI.Domain.Abstract;
using FWLibUI.Domain.Entities;

namespace FWLibUI.Domain.Concrete
{
    public class ImageRepository : IImageRepository
    {
        private readonly EFDbContext _context;

        public ImageRepository()
        {
            _context = EFDbContext.Create(EFDbContext.GetConnectionString());
        }

        public Result<IEnumerable<Image>> Images
        {
            get
            {
                try
                {
                    IEnumerable<Image> images = _context.Images;
                    return Result.Ok(images);
                }
                catch (Exception ex)
                {
                    return Result.Fail<IEnumerable<Image>>(ex.Message);
                }
            }
        }

        public Result Delete(Image image)
        {
            try
            {
                _context.Images.Remove(image);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.Message);
            }
        }

        public Result Add(Image image)
        {
            try
            {
                _context.Images.Add(image);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.Message);
            }
        }

        public Result Update(Image image)
        {
            try
            {
                _context.Images.AddOrUpdate(image);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.Message);
            }
        }

        public Result<IEnumerable<Image>> Find(Expression<Func<Image, bool>> predicate)
        {
            try
            {
                return Result.Ok<IEnumerable<Image>>(_context.Images.Where(predicate));
            }
            catch (Exception e)
            {
                return Result.Fail<IEnumerable<Image>>(e.Message);
            }
        }
    }
}