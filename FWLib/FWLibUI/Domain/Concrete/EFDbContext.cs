﻿using System;
using System.Data.Entity;
using System.Data.SQLite;
using System.Windows.Forms;
using Caliburn.Micro;
using System.Dynamic;
using System.IO;
using CSharpFunctionalExtensions;
using FWLibUI.Domain.Entities;
using FWLibUI.Domain.Services;
using FWLibUI.ViewModels;

namespace FWLibUI.Domain.Concrete
{
    public class EFDbContext : DbContext
    {
        public static string DatabaseName = string.Empty;
        public const string DefaultConnectionString = @"..\Database\testdatabase_v1.db";
        public static string UserConnectionString = @"..\Database\testdatabase_v1.db";
        private AppConfig _appConfig;

        private AppConfigService _appConfigService = new AppConfigService();
        private readonly SetupViewModel _setupViewModel;
        private readonly IWindowManager _windowManager;

        public EFDbContext(SetupViewModel setupViewModel, IWindowManager windowManager)
        {
            _setupViewModel = setupViewModel;
            _windowManager = windowManager;
        }

        public EFDbContext(string conn = DefaultConnectionString)
            : base(new SQLiteConnection() {ConnectionString = conn}, true)
        {
            if (conn.Length > 0)
            {
                DatabaseName = Database.Connection.ConnectionString.Substring(@"Data Source =.\".Length - 1);
            }
        }

        public static string GetConnectionString()
        {
            AppConfigService appConfigService = new AppConfigService();
            var result = appConfigService.ReadAppConfig(appConfigService.DefaultAppConfigPath);

            if (result.IsFailure)
            {
                return string.Empty;
            }

            return result.Value.DataBaseFilePath;
        }

        public static EFDbContext Create(string databasePath)
        {
            try
            {
                if (!File.Exists(databasePath))
                {
                    throw new FileNotFoundException();
                }
                var connString = $"Data Source={databasePath}";
                return new EFDbContext(connString);
            }
            catch (Exception e)
            {
                ShowSetupView();
                var messageServoce = new MessageService();
                messageServoce.ShowInfo("Restart application.");
                
                Application.Exit();

                throw;
            }
        }

        private static void ShowSetupView()
        {
            dynamic settings = new ExpandoObject();
            var appConfigService = new AppConfigService();
            var eventAgregator = new EventAggregator();
            var messageServoce = new MessageService();

            var setupViewModel = new SetupViewModel(appConfigService, eventAgregator, messageServoce);
            var windowManager = new WindowManager();
            windowManager.ShowDialog(setupViewModel, null, settings);
        }

        public DbSet<FWFile> FWFiles { get; set; }
        public DbSet<Image> Images { get; set; }
    }
}