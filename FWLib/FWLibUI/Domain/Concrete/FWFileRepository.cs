﻿using CSharpFunctionalExtensions;
using FWLibUI.Domain.Abstract;
using FWLibUI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;

namespace FWLibUI.Domain.Concrete
{
    public class FWFileRepository : IFWFileRepository
    {
        readonly EFDbContext _context;

        public FWFileRepository()
        {
            _context = EFDbContext.Create(EFDbContext.GetConnectionString());
        }

        /// <summary>
        /// Constructor for tests
        /// </summary>
        /// <param name="context"></param>
        public FWFileRepository(EFDbContext context)
        {
            _context = context;
        }

        public Result<IEnumerable<FWFile>> FWFiles
        {
            get
            {
                try
                {
                    IEnumerable<FWFile> fWFiles = _context.FWFiles;
                        //.Include(f => f.FileType)
                        //.Include(f => f.EngineType)
                        //.Include(f => f.CarBrand)
                        //.Include(f => f.CarModel)
                        //.Include(f => f.ControlUnit)
                        //.Include(f => f.ControlUnit)
                        //.Include(f => f.ControllerBrand)
                        //.Include(f => f.ControllerModel)
                        //.Include(f => f.MemoryType)
                        //.Include(f => f.TransmissionType);
                    return Result.Ok(fWFiles);
                }
                catch (Exception ex)
                {
                    return Result.Fail<IEnumerable<FWFile>>(ex.Message);
                }
            }
        }

        public Result Add(FWFile fwFile)
        {
            try
            {
                _context.FWFiles.Add(fwFile);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.Message);
            }
        }

        public Result Delete(FWFile fwFile)
        {
            try
            {
                _context.FWFiles.Remove(fwFile);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.Message);
            }
        }

        public Result Update(FWFile fwFile)
        {
            try
            {
                var file = _context.FWFiles.Find(fwFile.Id);
                _context.Entry(file).CurrentValues.SetValues(fwFile);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (Exception ex)
            {
                return Result.Fail(ex.Message);
            }
        }

        public Result<IEnumerable<FWFile>> Find(Expression<Func<FWFile, bool>> predicate)
        {
            try
            {
                return Result.Ok<IEnumerable<FWFile>>(_context.FWFiles.Where(predicate));
            }
            catch (Exception e)
            {
                return Result.Fail<IEnumerable<FWFile>>(e.Message);
            }
        }

        public Result<int> Count(Expression<Func<FWFile, bool>> predicate)
        {
            //var sql = @"SELECT Count(*) FROM Events";
            try
            {
                return Result.Ok<int>(_context.FWFiles.Count(predicate));
            }
            catch (DbUpdateException ex)
            {
                return Result.Fail<int>(ex.Message);
            }
        }
    }
}