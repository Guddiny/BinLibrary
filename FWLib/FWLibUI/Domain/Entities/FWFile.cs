﻿using System;

namespace FWLibUI.Domain.Entities
{
    public class FWFile
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Path { get; set; }

        public string FileType { get; set; }

        public string CarBrand { get; set; }

        public string CarModel { get; set; }

        public string EngineType { get; set; }

        public decimal EngineVolume { get; set; }

        public string TransmissionType { get; set; }

        public int Year { get; set; }

        public string ControlUnit { get; set; }
        
        public string ControlUnitType { get; set; }

        public string ControlUnitNumber { get; set; }

        public string ControllerBrand { get; set; }

        public string ControllerModel { get; set; }

        public string MemoryType { get; set; }

        public string VIN { get; set; }

        public byte[] FileData { get; set; }

        public byte[] MD5 { get; set; }

        public DateTime LastDate { get; set; }

        public string Note { get; set; }
    }
}