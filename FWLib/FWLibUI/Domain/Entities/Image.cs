﻿namespace FWLibUI.Domain.Entities
{
    public class Image
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public byte[] ImageData { get; set; }

        public byte[] MD5 { get; set; }
        
        public string ControlUnitName { get; set; }
        
        public string ControlUnitNumber { get; set; }
    }
}