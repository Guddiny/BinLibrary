﻿namespace FWLibUI.Domain.Entities
{
    public class AppConfig
    {
        public string DataBaseFilePath { get; set; }
        public string FilesBaseDir { get; set; }
        public string ImagesBaseDir { get; set; }
        public bool IsLog { get; set; }
    }
}