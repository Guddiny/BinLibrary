﻿namespace FWLibUI.Domain.Entities
{
    public class TechNote
    {
        public int Id { get; set; }
        
        public string Note { get; set; }
        
        public string ControlUnitName { get; set; }
        
        public string ControlUnitNumber { get; set; }
    }
}