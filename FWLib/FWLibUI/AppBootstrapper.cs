﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Threading;
using Caliburn.Micro;
using FWLibUI.Domain.Abstract;
using FWLibUI.Domain.Concrete;
using FWLibUI.Domain.Services;
using FWLibUI.ViewModels;
using NLog;

namespace FWLibUI
{
    public class AppBootstrapper : BootstrapperBase
    {
        private readonly SimpleContainer _container = new SimpleContainer();
        private Logger _log = NLog.LogManager.GetCurrentClassLogger();

        public AppBootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<MainViewModel>();
        }

        protected override void Configure()
        {
            _container.Instance(_container);

            _container
                .Singleton<IWindowManager, WindowManager>()
                .Singleton<IEventAggregator, EventAggregator>();

            _container
                .PerRequest<IFWFileRepository, FWFileRepository>()
                .PerRequest<IImageRepository, ImageRepository>();

            _container
                .Singleton<MainViewModel>()
                .Singleton<SetupViewModel>()
                .Singleton<EditFileViewModel>()
                .PerRequest<NewFileViewModel>()
                .PerRequest<ControlUnitInfoViewModel>();

            _container
                .Singleton<AppConfigService>()
                .Singleton<FWFileService>()
                .Singleton<ControlUnitService>()
                .Singleton<ControllerService>()
                .Singleton<CarService>()
                .Singleton<ImageService>()
                .Singleton<MessageService>();
        }

        protected override object GetInstance(Type serviceType, string key)
        {
            var instance = _container.GetInstance(serviceType, key);
            return instance;
        }

        protected override IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return _container.GetAllInstances(serviceType);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

#if !DEBUG
        protected override void OnUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;

            //MessageBox.Show("Oops... We have some problems. Sorry!", "An error as occurred", MessageBoxButton.OK);
            _log.Error(e.Exception);
        }
#endif
    }
}