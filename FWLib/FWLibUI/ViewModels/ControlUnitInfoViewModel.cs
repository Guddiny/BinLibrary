﻿using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using CSharpFunctionalExtensions;
using FWLibUI.Domain.Entities;
using FWLibUI.Domain.Services;
using FWLibUI.Helpers;
using NLog;

namespace FWLibUI.ViewModels
{
    public class ControlUnitInfoViewModel : Screen
    {
        private readonly ImageService _imageService;
        private readonly AppConfigService _appConfigService;
        private readonly MessageService _messageService;
        private Logger _log = NLog.LogManager.GetCurrentClassLogger();

        private Image _selectedImage;
        private FWFile _selectedFile;
        private string _currentImagePath;
        private int _currentImageIndex;
        private Visibility _progressVisibility = Visibility.Hidden;
        private bool _canNext;
        private bool _canPrev;

        // Properties ----------------------------------------------------------------------

        public BindableCollection<Image> Images { get; set; }

        public FWFile SelectedFile
        {
            get { return _selectedFile; }
            set
            {
                _selectedFile = value;
                NotifyOfPropertyChange(() => SelectedFile);
            }
        }

        public string CurrentImagePath
        {
            get { return _currentImagePath; }
            set
            {
                _currentImagePath = value;
                NotifyOfPropertyChange(() => CurrentImagePath);
            }
        }

        public int CurrentImageIndex
        {
            get { return _currentImageIndex; }
            set
            {
                _currentImageIndex = value;
                NotifyOfPropertyChange(() => CurrentImageIndex);
            }
        }

        public Image SelectedImage
        {
            get { return _selectedImage; }
            set
            {
                _selectedImage = value;
                NotifyOfPropertyChange(() => SelectedImage);
            }
        }

        public Visibility ProgressVisibility
        {
            get { return _progressVisibility; }
            set
            {
                _progressVisibility = value;
                NotifyOfPropertyChange(() => ProgressVisibility);
            }
        }

        public bool CanNext
        {
            get { return _canNext; }
            set
            {
                _canNext = value;
                NotifyOfPropertyChange(() => CanNext);
            }
        }

        public bool CanPrev
        {
            get { return _canPrev; }
            set
            {
                _canPrev = value;
                NotifyOfPropertyChange(() => CanPrev);
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public ControlUnitInfoViewModel(ImageService imageService, AppConfigService appConfigService,
            MessageService messageService)
        {
            _imageService = imageService;
            _appConfigService = appConfigService;
            _messageService = messageService;

            Images = new BindableCollection<Image>();
        }

        /// <summary>
        /// Update navigation button state
        /// </summary>
        private void UpdateNaviButtonState()
        {
            if (CurrentImageIndex == Images.Count - 1 && Images.Count == 1) // If we hane only one image in list
            {
                CanNext = false;
                CanPrev = false;
            }

            else if (CurrentImageIndex == 0)
            {
                CanNext = true;
                CanPrev = false;
            }
            else if (CurrentImageIndex == Images.Count - 1)
            {
                CanNext = false;
                CanPrev = true;
            }
            else
            {
                CanNext = true;
                CanPrev = true;
            }
        }

        /// <summary>
        /// Set currnt image property
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private async Task SetCurrentImage(int index)
        {
            if (Images.Count <= 0)
            {
                CurrentImagePath = string.Empty;
                return;
            }

            UpdateNaviButtonState();

            CurrentImagePath = string.Empty;
            var img = Images[index];
            string path = _appConfigService.TempFileFolderPath + "/" + img.MD5.BinToString() + "-" + img.Name;

            if (!File.Exists(path))
            {
                await Task.Run(() => { File.WriteAllBytes(path, img.ImageData); });
            }

            CurrentImagePath = path;
        }


        /// <summary>
        /// App load handler
        /// </summary>
        public void OnAppLoad()
        {
            Images.Clear();
            CurrentImageIndex = 0;

            var iamges = _imageService.GetImageByUnit(SelectedFile.ControlUnit, SelectedFile.ControlUnitNumber)
                .OnSuccess(async i =>
                {
                    Images.AddRange(i);
                    ProgressVisibility = Visibility.Visible;
                    await SetCurrentImage(CurrentImageIndex);
                    ProgressVisibility = Visibility.Hidden;
                });
        }

        /// <summary>
        /// Camcel button handler
        /// </summary>
        public void CancelHandler()
        {
            TryClose();
        }

        /// <summary>
        /// Add new file handler
        /// </summary>
        public void AddNewFile()
        {
            string path = FileHelper.ImageOpenDialog();

            if (!string.IsNullOrWhiteSpace(path))
            {
                var fileData = File.ReadAllBytes(path);
                var img = new Image()
                {
                    Name = Path.GetFileName(path),
                    ControlUnitName = SelectedFile.ControlUnit,
                    ControlUnitNumber = SelectedFile.ControlUnitNumber,
                    MD5 = MD5.Create().ComputeHash(fileData),
                    ImageData = fileData
                };

                var saveResult = _imageService.SaveImage(img)
                    .OnSuccess(() => OnAppLoad())
                    .OnFailure((e) =>
                    {
                        _messageService.ShowError(e);
                        _log.Error(e);
                    });
            }
        }

        /// <summary>
        /// Delete image handler
        /// </summary>
        public void DeleteImage()
        {
            if (Images.Count > 0)
            {
                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show(
                    $"Are you sure to delete the image?",
                    "Delete Confirmation", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    var deleteResult = _imageService.DeleteImage(Images[CurrentImageIndex])
                        .OnFailure((e) =>
                        {
                            _messageService.ShowError(e);
                            _log.Error(e);
                        })
                        .OnSuccess(() => OnAppLoad());
                }
            }
        }

        /// <summary>
        /// Save image handler
        /// </summary>
        public void SaveImageAs()
        {
            if (Images.Count > 0)
            {
                string filePath = FileHelper.ImageSaveDialog(Images[CurrentImageIndex].Name);
                if (!string.IsNullOrWhiteSpace(filePath))
                {
                    File.WriteAllBytes(filePath, Images[CurrentImageIndex].ImageData);
                }
            }
        }

        /// <summary>
        /// Open current image by external tool
        /// </summary>
        public void OpenInExternalTool()
        {
            Process.Start(CurrentImagePath);
        }

        /// <summary>
        /// Set next image from image list
        /// </summary>
        public async void NextImage()
        {
            CurrentImageIndex++;
            await SetCurrentImage(CurrentImageIndex);
        }

        /// <summary>
        /// Set previous image from image list
        /// </summary>
        public async void PrevImage()
        {
            CurrentImageIndex--;
            await SetCurrentImage(CurrentImageIndex);
        }
    }
}