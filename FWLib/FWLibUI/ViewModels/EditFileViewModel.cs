﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Caliburn.Micro;
using FWLibUI.Domain.Entities;
using FWLibUI.Domain.Services;
using System.Windows.Forms;
using CSharpFunctionalExtensions;
using FWLibUI.Helpers;
using NLog;
using Screen = Caliburn.Micro.Screen;

namespace FWLibUI.ViewModels
{
    public class EditFileViewModel : Screen
    {
        public BindableCollection<FWFile> FileList { get; set; }
        public BindableCollection<string> FileNames { get; set; }
        public BindableCollection<string> CarBrandList { get; set; }
        public BindableCollection<string> TransmissionTypeList { get; set; }
        public BindableCollection<string> EngineTypeList { get; set; }
        public BindableCollection<string> ControlUnitList { get; set; }
        public BindableCollection<string> FileTypeList { get; set; }
        public BindableCollection<string> CarModelList { get; set; }
        public BindableCollection<string> ControlUnitTypeList { get; set; }
        public BindableCollection<string> VINList { get; set; }
        public BindableCollection<string> MemoryTypeList { get; set; }
        public BindableCollection<string> ECUNumberList { get; set; }
        public BindableCollection<string> ControllerBrandList { get; set; }
        public BindableCollection<string> ControllerModelList { get; set; }
        public FWFile SelectedFile { get; set; }


        private readonly FWFileService _fileService;
        private readonly CarService _carService;
        private readonly ControlUnitService _unitService;
        private readonly ControllerService _controllerService;
        private readonly AppConfigService _appConfigService;
        private readonly MessageService _messageService;
        private readonly IEventAggregator _eventAggregator;
        private readonly AppConfig _appConfig;
        private Logger _log = NLog.LogManager.GetCurrentClassLogger();

        private string _selectedTransmissionType = string.Empty;
        private string _selectedCarBrand = string.Empty;
        private string _selectedControlUnit = string.Empty;
        private string _selectedFileType = string.Empty;
        private string _selectedCarModel = string.Empty;
        private string _selectedControlUnitType = string.Empty;
        private string _selectedVIN = string.Empty;
        private string _selectedMemoryType = string.Empty;
        private string _selectedECUNumber = string.Empty;
        private string _selectedControllerModel = string.Empty;
        private string _selectedControllerBrand = string.Empty;
        private decimal _selectedEngineVolume = 0;
        private string _selectedEngineType = string.Empty;
        private string _filePath = string.Empty;
        private int _selectedYear = 0;
        private string _selectedNote = string.Empty;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fileService"></param>
        /// <param name="carService"></param>
        /// <param name="unitService"></param>
        /// <param name="controllerService"></param>
        /// <param name="appConfigService"></param>
        /// <param name="messageService"></param>
        /// <param name="eventAggregator"></param>
        public EditFileViewModel(FWFileService fileService, CarService carService, ControlUnitService unitService,
            ControllerService controllerService, AppConfigService appConfigService, MessageService messageService,
            IEventAggregator eventAggregator)
        {
            _fileService = fileService;
            _carService = carService;
            _unitService = unitService;
            _controllerService = controllerService;
            _appConfigService = appConfigService;
            _messageService = messageService;
            _eventAggregator = eventAggregator;


            var appConfigResult = _appConfigService.ReadAppConfig(_appConfigService.DefaultAppConfigPath);
            if (appConfigResult.IsSuccess)
            {
                _appConfig = appConfigResult.Value;
            }

            FileList = new BindableCollection<FWFile>();
            CarBrandList = new BindableCollection<string>();
            TransmissionTypeList = new BindableCollection<string>();
            EngineTypeList = new BindableCollection<string>();
            ControlUnitList = new BindableCollection<string>();
            FileTypeList = new BindableCollection<string>();
            CarModelList = new BindableCollection<string>();
            ControlUnitTypeList = new BindableCollection<string>();
            VINList = new BindableCollection<string>();
            MemoryTypeList = new BindableCollection<string>();
            ECUNumberList = new BindableCollection<string>();
            ControllerBrandList = new BindableCollection<string>();
            ControllerModelList = new BindableCollection<string>();
        }

        //  Properties ---------------------------------------------------------

        public string SelectedFilePath
        {
            get { return _filePath; }
            set
            {
                _filePath = value;
                NotifyOfPropertyChange(() => SelectedFilePath);
            }
        }

        public string SelectedFileType
        {
            get { return _selectedFileType; }
            set
            {
                _selectedFileType = value;
                NotifyOfPropertyChange(() => SelectedFileType);
            }
        }

        public string SelectedControlUnitType
        {
            get { return _selectedControlUnitType; }
            set
            {
                _selectedControlUnitType = value;
                NotifyOfPropertyChange(() => SelectedControlUnitType);
            }
        }

        public string SelectedCarBrand
        {
            get { return _selectedCarBrand; }
            set
            {
                _selectedCarBrand = value;
                NotifyOfPropertyChange(() => SelectedCarBrand);
            }
        }

        public string SelectedCarModel
        {
            get { return _selectedCarModel; }
            set
            {
                _selectedCarModel = value;
                NotifyOfPropertyChange(() => SelectedCarModel);
            }
        }

        public int SelectedYear
        {
            get { return _selectedYear; }
            set
            {
                _selectedYear = value;
                NotifyOfPropertyChange(() => SelectedYear);
            }
        }

        public string SelectedEngineType
        {
            get { return _selectedEngineType; }
            set
            {
                _selectedEngineType = value;
                NotifyOfPropertyChange(() => SelectedEngineType);
            }
        }

        public decimal SelectedEngineVolume
        {
            get { return _selectedEngineVolume; }
            set
            {
                _selectedEngineVolume = value;
                NotifyOfPropertyChange(() => SelectedEngineVolume);
            }
        }

        public string SelectedTransmissionType
        {
            get { return _selectedTransmissionType; }
            set
            {
                _selectedTransmissionType = value;
                NotifyOfPropertyChange(() => SelectedTransmissionType);
            }
        }

        public string SelectedControlUnit
        {
            get { return _selectedControlUnit; }
            set
            {
                _selectedControlUnit = value;
                NotifyOfPropertyChange(() => SelectedControlUnit);
            }
        }

        public string SelectedECUNumber
        {
            get { return _selectedECUNumber; }
            set
            {
                _selectedECUNumber = value;
                NotifyOfPropertyChange(() => SelectedECUNumber);
            }
        }

        public string SelectedControllerBrand
        {
            get { return _selectedControllerBrand; }
            set
            {
                _selectedControllerBrand = value;
                NotifyOfPropertyChange(() => SelectedControllerBrand);
            }
        }

        public string SelectedControllerModel
        {
            get { return _selectedControllerModel; }
            set
            {
                _selectedControllerModel = value;
                NotifyOfPropertyChange(() => SelectedControllerModel);
            }
        }

        public string SelectedMemoryType
        {
            get { return _selectedMemoryType; }
            set
            {
                _selectedMemoryType = value;
                NotifyOfPropertyChange(() => SelectedMemoryType);
            }
        }

        public string SelectedVIN
        {
            get { return _selectedVIN; }
            set
            {
                _selectedVIN = value;
                NotifyOfPropertyChange(() => SelectedVIN);
            }
        }

        public string SelectedNote
        {
            get { return _selectedNote; }
            set
            {
                _selectedNote = value;
                NotifyOfPropertyChange(() => SelectedNote);
            }
        }

        /// <summary>
        /// App load handler
        /// </summary>
        public async void OnAppLoad()
        {
            await Update();
        }

        /// <summary>
        /// Update all item sources and entities
        /// </summary>
        public async Task Update()
        {
            await Task.Factory.StartNew(() =>
            {
                FileTypeList.Clear();
                ControlUnitList.Clear();
                EngineTypeList.Clear();
                TransmissionTypeList.Clear();
                CarBrandList.Clear();
                FileList.Clear();
                ControlUnitTypeList.Clear();
                VINList.Clear();
                MemoryTypeList.Clear();
                ECUNumberList.Clear();
                ControllerBrandList.Clear();
                ControllerModelList.Clear();

                SelectedTransmissionType = SelectedFile.TransmissionType;
                SelectedCarBrand = SelectedFile.CarBrand;
                SelectedControlUnit = SelectedFile.ControlUnit;
                SelectedFileType = SelectedFile.FileType;
                SelectedCarModel = SelectedFile.CarModel;
                SelectedControlUnitType = SelectedFile.ControlUnitType;
                SelectedVIN = SelectedFile.VIN;
                SelectedMemoryType = SelectedFile.MemoryType;
                SelectedECUNumber = SelectedFile.ControlUnitNumber;
                SelectedControllerModel = SelectedFile.ControllerModel;
                SelectedControllerBrand = SelectedFile.ControllerBrand;
                SelectedEngineVolume = SelectedFile.EngineVolume;
                SelectedEngineType = SelectedFile.EngineType;
                SelectedFilePath = SelectedFile.Name;
                SelectedYear = SelectedFile.Year;
                SelectedNote = SelectedFile.Note;

                var files = _fileService.GetAllFiles(0, 100).Value;
                var carBrands = _carService.GetAllCarBrands().Value;
                var transmTypes = _carService.GetAllTransmissionTypes().Value;
                var engineTypes = _carService.GetAllEngineTypes().Value;
                var controlUnits = _unitService.GetAllControlUnits().Value;
                var fileTypes = _fileService.GetAllFileTypes().Value;
                var controlUnitTypes = _unitService.GetAllControlUnitTypes().Value;
                var vins = _fileService.GetAllVIN().Value;
                var memoryTypes = _fileService.GetAllMemoryTypes().Value;
                var ecuNumbers = _fileService.GetAllECUNumbers().Value;
                var controllerBrands = _controllerService.GetAllControllerBrands().Value;
                var controllerModels = _controllerService.GetAllControllerModels().Value;

                FileTypeList.AddRange(fileTypes);
                ControlUnitList.AddRange(controlUnits);
                EngineTypeList.AddRange(engineTypes);
                TransmissionTypeList.AddRange(transmTypes);
                CarBrandList.AddRange(carBrands);
                FileList.AddRange(files);
                ControlUnitTypeList.AddRange(controlUnitTypes);
                VINList.AddRange(vins);
                MemoryTypeList.AddRange(memoryTypes);
                ECUNumberList.AddRange(ecuNumbers);
                ControllerBrandList.AddRange(controllerBrands);
                ControllerModelList.AddRange(controllerModels);
            });
        }

        /// <summary>
        /// Update Car Models list
        /// </summary>
        /// <param name="carBrand"></param>
        public void UpdateModelList(string carBrand)
        {
            CarModelList.Clear();
            if (carBrand != null)
            {
                CarModelList.AddRange(_carService.GetRelatedModels(carBrand).Value);
            }
        }

        /// <summary>
        /// Open file dialog
        /// </summary>
        /// <returns></returns>
        private string FileDialog()
        {
            string fwFilePath = string.Empty;
            var fd = new System.Windows.Forms.OpenFileDialog();

            fd.InitialDirectory = @"C:\";
            fd.Title = @"Browse binary files";
            fd.CheckFileExists = true;
            fd.CheckPathExists = true;
            fd.DefaultExt = "txt";
            fd.Filter = @"Binary file |*.bin|All files |*.*";
            fd.FilterIndex = 1;
            fd.RestoreDirectory = true;
            fd.ReadOnlyChecked = true;
            fd.ShowReadOnly = true;

            if (fd.ShowDialog() == DialogResult.OK)
            {
                fwFilePath = fd.FileName;
            }

            return fwFilePath;
        }

        /// <summary>
        /// Camcel button handler
        /// </summary>
        public void CancelHandler()
        {
            TryClose();
        }

        /// <summary>
        /// Handler for save file command
        /// </summary>
        public void SaveHandler()
        {
            var newFile = new FWFile()
            {
                Id = SelectedFile.Id,
                Path = "",
                Name = "",
                FileType = _selectedFileType,
                CarBrand = SelectedCarBrand,
                CarModel = SelectedCarModel,
                EngineType = _selectedEngineType,
                EngineVolume = _selectedEngineVolume,
                TransmissionType = _selectedTransmissionType,
                ControlUnitType = _selectedControlUnitType,
                ControlUnit = _selectedControlUnit,
                ControlUnitNumber = _selectedECUNumber,
                ControllerBrand = SelectedControllerBrand,
                ControllerModel = SelectedControllerModel,
                MemoryType = _selectedMemoryType,
                Year = _selectedYear,
                VIN = _selectedVIN,
                FileData = SelectedFile.FileData,
                MD5 = SelectedFile.MD5,
                LastDate = DateTime.Now,
                Note = _selectedNote
            };

            PerformFileOperation(newFile);
        }

        /// <summary>
        /// Perform File Operation (save or update file)
        /// </summary>
        /// <param name="newFile"></param>
        private void PerformFileOperation(FWFile newFile)
        {
            if (SelectedFilePath != SelectedFile.Name)
            {
                if (File.Exists(SelectedFilePath))
                {
                    newFile.Name = Path.GetFileName(SelectedFilePath);
                    newFile.FileData = File.ReadAllBytes(SelectedFilePath);
                    newFile.MD5 = MD5.Create().ComputeHash(newFile.FileData);

                    var result = FileHelper.ValidateFile(newFile)
                        .OnFailure((e) =>
                        {
                            _messageService.ShowError(e);
                            return;
                        });
                }
                else
                {
                    _messageService.ShowError("File not exist");
                    return;
                }
            }
            else
            {
                newFile.Path = SelectedFile.Path;
                newFile.Name = SelectedFile.Name;
            }

            _fileService.UpdatFile(newFile)
                .OnSuccess(() =>
                {
                    _eventAggregator.PublishOnUIThread(true);
                    TryClose();
                })
                .OnFailure((result) => { _messageService.ShowError(result); });
        }

        /// <summary>
        /// Set selected file path
        /// </summary>
        public void SetFilePathAndName()
        {
            SelectedFilePath = FileDialog();
        }

        public void TetxInputHandler(string engineTypeName)
        {
        }
    }
}