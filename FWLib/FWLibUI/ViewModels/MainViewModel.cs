﻿using System.Diagnostics;
using Caliburn.Micro;
using FWLibUI.Domain.Concrete;
using FWLibUI.Domain.Entities;
using FWLibUI.Domain.Services;
using FWLibUI.Views;
using NLog;
using System.Dynamic;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using CSharpFunctionalExtensions;
using FWLibUI.Domain.Abstract;
using FWLibUI.Helpers;

namespace FWLibUI.ViewModels
{
    public class MainViewModel : Screen, IHandle<bool>
    {
        public BindableCollection<FWFile> FileList { get; set; }
        public BindableCollection<string> FileNames { get; set; }
        public BindableCollection<string> CarBrandList { get; set; }
        public BindableCollection<string> TransmissionTypeList { get; set; }
        public BindableCollection<string> EngineTypeList { get; set; }
        public BindableCollection<string> ControlUnitList { get; set; }
        public BindableCollection<string> FileTypeList { get; set; }
        public BindableCollection<string> CarModelList { get; set; }
        public BindableCollection<string> ControlUnitTypeList { get; set; }
        public BindableCollection<string> VINList { get; set; }
        public BindableCollection<string> MemoryTypeList { get; set; }
        public BindableCollection<string> ECUNumberList { get; set; }
        public BindableCollection<string> ControllerBrandList { get; set; }
        public BindableCollection<string> ControllerModelList { get; set; }


        private int _itemsCount = 0;
        private int _totalItemsCount = 0;
        private string _selectedFileNote = string.Empty;
        private string _databaseName = string.Empty;
        private Visibility _progressVisibility = Visibility.Hidden;
        private AppConfig _appConfig;
        private FWFile _selectedFile;

        private string _selectedTransmissionType = string.Empty;
        private string _selectedCarBrand = string.Empty;
        private string _selectedControlUnit = string.Empty;
        private string _selectedFileType = string.Empty;
        private string _selectedCarModel = string.Empty;
        private string _selectedControlUnitType = string.Empty;
        private string _selectedVIN = string.Empty;
        private string _selectedMemoryType = string.Empty;
        private string _selectedECUNumber = string.Empty;
        private string _selectedControllerModel = string.Empty;
        private string _selectedControllerBrand = string.Empty;
        private decimal _selectedEngineVolume = 0;
        private string _selectedEngineType = string.Empty;

        private AboutWindow _aboutWindow;
        private readonly SetupViewModel _setupViewModel;
        private readonly IWindowManager _windowManager;
        private readonly IEventAggregator _eventAggregator;
        private readonly AppConfigService _appConfigService;
        private readonly ControlUnitInfoViewModel _infoViewModel;
        private readonly NewFileViewModel _newFileViewModel;
        private readonly EditFileViewModel _editFileViewModel;
        private readonly FWQueryBuilder _queryBuilder;
        private Logger _log = NLog.LogManager.GetCurrentClassLogger();

        private FWFileService _fileService;
        private MessageService _messageService;
        private CarService _carService;
        private ControlUnitService _unitService;
        private ControllerService _controllerService;

        /* Properties */

        public int ItemsCount
        {
            get { return _itemsCount; }
            set
            {
                _itemsCount = value;
                NotifyOfPropertyChange(() => ItemsCount);
            }
        }

        public int TotalItemsCount
        {
            get { return _totalItemsCount; }
            set
            {
                _totalItemsCount = value;
                NotifyOfPropertyChange(() => TotalItemsCount);
            }
        }

        public string SelectedFileNote
        {
            get { return _selectedFileNote; }
            set
            {
                _selectedFileNote = value;
                NotifyOfPropertyChange(() => SelectedFileNote);
            }
        }

        public string DatabaseName
        {
            get { return _databaseName; }
            set
            {
                _databaseName = value;
                NotifyOfPropertyChange(() => DatabaseName);
            }
        }

        public Visibility ProgressVisibility
        {
            get { return _progressVisibility; }
            set
            {
                _progressVisibility = value;
                NotifyOfPropertyChange(() => ProgressVisibility);
            }
        }

        // ---------------------------------------------------------------------

        public string SelectedFileType
        {
            get { return _selectedFileType; }
            set
            {
                _selectedFileType = value;
                NotifyOfPropertyChange(() => SelectedFileType);
            }
        }

        public string SelectedCarBrand
        {
            get { return _selectedCarBrand; }
            set
            {
                _selectedCarBrand = value;
                NotifyOfPropertyChange(() => SelectedCarBrand);
            }
        }

        public string SelectedCarModel
        {
            get { return _selectedCarModel; }
            set
            {
                _selectedCarModel = value;
                NotifyOfPropertyChange(() => SelectedCarModel);
            }
        }


        public string SelectedEngineType
        {
            get { return _selectedEngineType; }
            set
            {
                _selectedEngineType = value;
                NotifyOfPropertyChange(() => SelectedEngineType);
            }
        }

//        public int SelectedYear
//        {
//            get { return _selectedYear; }
//            set
//            {
//                _selectedYear = value;
//                NotifyOfPropertyChange(() => SelectedYear);
//            }
//        }

        public decimal SelectedEngineVolume
        {
            get { return _selectedEngineVolume; }
            set
            {
                _selectedEngineVolume = value;
                NotifyOfPropertyChange(() => SelectedEngineVolume);
            }
        }

        public string SelectedTransmissionType
        {
            get { return _selectedTransmissionType; }
            set
            {
                _selectedTransmissionType = value;
                NotifyOfPropertyChange(() => SelectedTransmissionType);
            }
        }

        public string SelectedControlUnitType
        {
            get { return _selectedControlUnitType; }
            set
            {
                _selectedControlUnitType = value;
                NotifyOfPropertyChange(() => SelectedControlUnitType);
            }
        }

        public string SelectedControlUnit
        {
            get { return _selectedControlUnit; }
            set
            {
                _selectedControlUnit = value;
                NotifyOfPropertyChange(() => SelectedControlUnit);
            }
        }

        public string SelectedECUNumber
        {
            get { return _selectedECUNumber; }
            set
            {
                _selectedECUNumber = value;
                NotifyOfPropertyChange(() => SelectedECUNumber);
            }
        }

        public string SelectedControllerBrand
        {
            get { return _selectedControllerBrand; }
            set
            {
                _selectedControllerBrand = value;
                NotifyOfPropertyChange(() => SelectedControllerBrand);
            }
        }

        public string SelectedControllerModel
        {
            get { return _selectedControllerModel; }
            set
            {
                _selectedControllerModel = value;
                NotifyOfPropertyChange(() => SelectedControllerModel);
            }
        }

        public string SelectedMemoryType
        {
            get { return _selectedMemoryType; }
            set
            {
                _selectedMemoryType = value;
                NotifyOfPropertyChange(() => SelectedMemoryType);
            }
        }

        public string SelectedVIN
        {
            get { return _selectedVIN; }
            set
            {
                _selectedVIN = value;
                NotifyOfPropertyChange(() => SelectedVIN);
            }
        }


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="setupViewModel"></param>
        /// <param name="windowManager"></param>
        /// <param name="appConfigService"></param>
        /// <param name="eventAggregator"></param>
        public MainViewModel(SetupViewModel setupViewModel, IWindowManager windowManager,
            AppConfigService appConfigService, IEventAggregator eventAggregator, NewFileViewModel newFileViewModel,
            EditFileViewModel editFileViewModel, MessageService messageService, ControlUnitInfoViewModel infoViewModel)
        {
            _setupViewModel = setupViewModel;
            _newFileViewModel = newFileViewModel;
            _windowManager = windowManager;
            _editFileViewModel = editFileViewModel;
            _messageService = messageService;
            _infoViewModel = infoViewModel;
            _appConfigService = new AppConfigService();
            _eventAggregator = eventAggregator;
            _eventAggregator.Subscribe(this);
            _queryBuilder = new FWQueryBuilder();

            FileList = new BindableCollection<FWFile>();
            CarBrandList = new BindableCollection<string>();
            TransmissionTypeList = new BindableCollection<string>();
            EngineTypeList = new BindableCollection<string>();
            ControlUnitList = new BindableCollection<string>();
            FileTypeList = new BindableCollection<string>();
            CarModelList = new BindableCollection<string>();
            ControlUnitTypeList = new BindableCollection<string>();
            VINList = new BindableCollection<string>();
            MemoryTypeList = new BindableCollection<string>();
            ECUNumberList = new BindableCollection<string>();
            ControllerBrandList = new BindableCollection<string>();
            ControllerModelList = new BindableCollection<string>();

            _log.Info("------------------------------- APP STARTED---------------------");
        }

        /// <summary>
        /// Update all item sources and entities
        /// </summary>
        public async void Update()
        {
            ProgressVisibility = Visibility.Visible;
            await Task.Factory.StartNew(() =>
            {
                FileTypeList.Clear();
                ControlUnitList.Clear();
                EngineTypeList.Clear();
                TransmissionTypeList.Clear();
                CarBrandList.Clear();
                FileList.Clear();
                ControlUnitTypeList.Clear();
                VINList.Clear();
                MemoryTypeList.Clear();
                ECUNumberList.Clear();
                ControllerBrandList.Clear();
                ControllerModelList.Clear();

                var files = _fileService.GetAllFiles(0, 100).Value;
                var carBrands = _carService.GetAllCarBrands().Value;
                var transmTypes = _carService.GetAllTransmissionTypes().Value;
                var engineTypes = _carService.GetAllEngineTypes().Value;
                var controlUnits = _unitService.GetAllControlUnits().Value;
                var fileTypes = _fileService.GetAllFileTypes().Value;
                var controlUnitTypes = _unitService.GetAllControlUnitTypes().Value;
                var vins = _fileService.GetAllVIN().Value;
                var memoryTypes = _fileService.GetAllMemoryTypes().Value;
                var ecuNumbers = _fileService.GetAllECUNumbers().Value;
                var controllerBrands = _controllerService.GetAllControllerBrands().Value;
                var controllerModels = _controllerService.GetAllControllerModels().Value;

                FileTypeList.AddRange(fileTypes);
                ControlUnitList.AddRange(controlUnits);
                EngineTypeList.AddRange(engineTypes);
                TransmissionTypeList.AddRange(transmTypes);
                CarBrandList.AddRange(carBrands);
                FileList.AddRange(files);
                ControlUnitTypeList.AddRange(controlUnitTypes);
                VINList.AddRange(vins);
                MemoryTypeList.AddRange(memoryTypes);
                ECUNumberList.AddRange(ecuNumbers);
                ControllerBrandList.AddRange(controllerBrands);
                ControllerModelList.AddRange(controllerModels);
            });
            ProgressVisibility = Visibility.Hidden;

            ItemsCount = FileList.Count;
            TotalItemsCount = _fileService.GetTotalItemsCount().Value;
            DatabaseName = EFDbContext.DatabaseName;
        }

        /// <summary>
        /// Update Car Models list
        /// </summary>
        /// <param name="carBrand"></param>
        public void UpdateModelList(string carBrand)
        {
            CarModelList.Clear();
            if (carBrand != null)
            {
                CarModelList.AddRange(_carService.GetRelatedModels(carBrand).Value);
            }
        }

        /// <summary>
        /// Handler for item selection in ListView
        /// </summary>
        /// <param name="fWFile"></param>
        public void FileListSelectionHandler(FWFile fWFile)
        {
            if (fWFile != null)
            {
                SelectedFileNote = fWFile.Note;
                _editFileViewModel.SelectedFile = fWFile;
                _selectedFile = fWFile;
                _infoViewModel.SelectedFile = fWFile;
            }
        }

        /// <summary>
        /// Handle doublw click on selected file and open it by external programm
        /// </summary>
        /// <param name="fWFile"></param>
        public void OpenSelectedFWFile(FWFile fWFile)
        {
            if (fWFile != null)
            {
                File.WriteAllBytes(
                    _appConfigService.TempFileFolderPath + "/" + fWFile.MD5.BinToString() + "-" + fWFile.Name,
                    fWFile.FileData);
                Process.Start(_appConfigService.TempFileFolderPath + "/" + fWFile.MD5.BinToString() + "-" +
                              fWFile.Name);
            }
        }

        /// <summary>
        /// Some actions when add load
        /// </summary>
        public void OnAppLoad()
        {
            var configResult = _appConfigService.ReadAppConfig(_appConfigService.DefaultAppConfigPath);

            if (configResult.IsFailure || string.IsNullOrWhiteSpace(configResult.Value.DataBaseFilePath))
            {
                var result = _appConfigService.SaveAppConfig(new AppConfig()
                {
                    DataBaseFilePath = @"..\Database\testdatabase_v1.db",
                    FilesBaseDir = @"..\Files",
                    ImagesBaseDir = @"..\Images"
                }, _appConfigService.DefaultAppConfigPath);

                if (result.IsSuccess)
                {
                    OnAppLoad();
                }
                else
                {
                    _messageService.ShowError(result.Error);
                }
            }
            else
            {
                _appConfig = configResult.Value;
                AppInit();
            }
        }

        /// <summary>
        /// Show Avout View
        /// </summary>
        public void ShowAboutView()
        {
            _aboutWindow = new AboutWindow();
            _aboutWindow.Owner = Application.Current.MainWindow;
            _aboutWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            _aboutWindow.ShowDialog();
        }

        /// <summary>
        /// Show Setup View
        /// </summary>
        public void ShowSetupView()
        {
            dynamic settings = new ExpandoObject();
            settings.Owner = Application.Current.MainWindow;
            settings.WindowStartupLocation = WindowStartupLocation.CenterOwner;

            _windowManager.ShowDialog(_setupViewModel, null, settings);
        }

        /// <summary>
        /// Show ECU info window
        /// </summary>
        public void EcuInfo()
        {
            dynamic settings = new ExpandoObject();
            //settings.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            if (_infoViewModel.SelectedFile != null)
            {
                _windowManager.ShowDialog(_infoViewModel, null, settings);
            }
        }

        /// <summary>
        /// Show add new file view
        /// </summary>
        public void AddNewFile()
        {
            dynamic settings = new ExpandoObject();
            settings.Owner = Application.Current.MainWindow;
            settings.WindowStartupLocation = WindowStartupLocation.CenterOwner;

            _windowManager.ShowDialog(_newFileViewModel, null, settings);
        }

        /// <summary>
        /// Edit selected file
        /// </summary>
        public void EditFile()
        {
            if (_editFileViewModel.SelectedFile != null)
            {
                dynamic settings = new ExpandoObject();
                settings.Owner = Application.Current.MainWindow;
                settings.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                _windowManager.ShowDialog(_editFileViewModel, null, settings);
            }
        }

        /// <summary>
        /// Delete selected file
        /// </summary>
        public void DeleteFile()
        {
            if (_selectedFile != null)
            {
                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show(
                    $"Are you sure to delete \"{_editFileViewModel.SelectedFile.Name}\" file?",
                    "Delete Confirmation", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    _fileService.DeleteFile(_selectedFile)
                        .OnSuccess(() =>
                        {
                            _selectedFile = null;
                            _editFileViewModel.SelectedFile = null;
                            AppInit();
                        })
                        .OnFailure((result) => _messageService.ShowError(result));
                }
            }
        }

        /// <summary>
        /// Search file in database
        /// </summary>
        public void Search()
        {
            var queryString = _queryBuilder.BuildQueryString(SelectedFileType, SelectedCarBrand, SelectedCarModel,
                SelectedEngineType, SelectedEngineVolume, SelectedTransmissionType, SelectedControlUnitType,
                SelectedControlUnit, SelectedECUNumber, SelectedControllerBrand, SelectedControllerModel,
                SelectedMemoryType, SelectedVIN);
            var findResult = _queryBuilder.GenerateQueryExpression(queryString)
                .OnSuccess(exp => _fileService.FindFiles(exp));

            if (findResult.IsSuccess)
            {
                FileList.Clear();
                FileList.AddRange(findResult.Value);
            }
        }

        /// <summary>
        /// Save selected file "As" - in custom folder
        /// </summary>
        public void SaveFileAs()
        {
            if (_editFileViewModel.SelectedFile != null)
            {
                string filePath = FileHelper.FileSaveDialog(_editFileViewModel.SelectedFile.Name);
                if (!string.IsNullOrWhiteSpace(filePath))
                {
                    File.WriteAllBytes(filePath, _editFileViewModel.SelectedFile.FileData);
                }
            }
        }

        /// <summary>
        /// Init application main entites 
        /// </summary>
        private void AppInit()
        {
            EFDbContext.UserConnectionString = _appConfig.DataBaseFilePath;

            _fileService = new FWFileService();
            _carService = new CarService();
            _unitService = new ControlUnitService();
            _controllerService = new ControllerService();

            Update();
        }

        /// <summary>
        /// App closing handler
        /// </summary>
        public void OnAppClosing()
        {
            _log.Info("------------------------------- APP SHUTDOWN---------------------");
        }

        /// <summary>
        /// Handler for message from Setup View
        /// </summary>
        /// <param name="message"></param>
        public void Handle(bool message)
        {
            if (message)
            {
                OnAppLoad();

                //EFDbContext context = EFDbContext.Create(@".\testdatabsae_v1.db");
                //DbInitializer dbInitializer = new DbInitializer();
                //dbInitializer.Initialize(context);
            }
        }
    }
}