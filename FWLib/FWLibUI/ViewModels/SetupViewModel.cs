﻿using System;
using System.Windows.Forms;
using Caliburn.Micro;
using FWLibUI.Domain.Entities;
using FWLibUI.Domain.Services;
using FWLibUI.Helpers;
using Screen = Caliburn.Micro.Screen;


namespace FWLibUI.ViewModels
{
    public class SetupViewModel : Screen
    {       
        private string _dataBaseFilePath = string.Empty;
        private string _filesBaseDir = string.Empty;
        private string _imagesBaseDir = string.Empty;

        private readonly AppConfigService _appConfigService;
        private readonly IEventAggregator _eventAggregator;
        private readonly MessageService _messageService;

        public string DataBaseFilePath
        {
            get { return _dataBaseFilePath; }
            set
            {
                _dataBaseFilePath = value;
                NotifyOfPropertyChange(() => DataBaseFilePath);
            }
        }

        public string FilesBaseDir
        {
            get { return _filesBaseDir; }
            set
            {
                _filesBaseDir = value;
                NotifyOfPropertyChange(() => FilesBaseDir);
            }
        }

        public string ImagesBaseDir
        {
            get { return _imagesBaseDir; }
            set
            {
                _imagesBaseDir = value;
                NotifyOfPropertyChange(() => ImagesBaseDir);
            }
        }

        private string FileDialog()
        {
            string fwFilePath = string.Empty;
            var fd = new System.Windows.Forms.OpenFileDialog();

            fd.InitialDirectory = @"C:\";
            fd.Title = "Browse Text Files";
            fd.CheckFileExists = true;
            fd.CheckPathExists = true;
            fd.DefaultExt = "txt";
            fd.Filter = "Database files |*.db|All files |*.*";
            fd.FilterIndex = 1;
            fd.RestoreDirectory = true;
            fd.ReadOnlyChecked = true;
            fd.ShowReadOnly = true;

            if (fd.ShowDialog() == DialogResult.OK)
            {
                fwFilePath = fd.FileName;
            }

            return fwFilePath;
        }

        private string FolderDialog()
        {
            string directory = string.Empty;
            var fd = new FolderBrowserDialog();

            DialogResult fdresult = fd.ShowDialog();

            if (fdresult == DialogResult.OK && !string.IsNullOrWhiteSpace(fd.SelectedPath))
            {
                directory = fd.SelectedPath;
            }

            return directory;
        }

        public void SetDataBaseFilePath()
        {
            DataBaseFilePath = FileDialog();
        }

        public void SetFilesBaseDir()
        {
            FilesBaseDir = FolderDialog();
        }

        public void SetImagesBaseDir()
        {
            ImagesBaseDir = FolderDialog();
        }

        public void OKButtonHandler()
        {
            AppConfig appConfig = new AppConfig();
            appConfig.DataBaseFilePath = this.DataBaseFilePath;
            appConfig.FilesBaseDir = this.FilesBaseDir;
            appConfig.ImagesBaseDir = this.ImagesBaseDir;

            var saveResult = _appConfigService.SaveAppConfig(appConfig, _appConfigService.DefaultAppConfigPath);

            if (saveResult.IsFailure)
            {
                //TODO add failure handler
            }
            else
            {
                _eventAggregator.PublishOnUIThread(true);
                TryClose();
            }  
        }

        public void CancelButtonHandler()
        {
            TryClose();
        }

        public void ReadConfig()
        {
            var config = _appConfigService.ReadAppConfig(_appConfigService.DefaultAppConfigPath);

            if (config.IsSuccess)
            {
                DataBaseFilePath = config.Value.DataBaseFilePath;
                FilesBaseDir = config.Value.FilesBaseDir;
                ImagesBaseDir = config.Value.ImagesBaseDir;
            }
        }

        public SetupViewModel(AppConfigService appConfigService, IEventAggregator eventAggregator, MessageService messageService)
        {
            _appConfigService = appConfigService;
            _eventAggregator = eventAggregator;
            _messageService = messageService;
        }

        public void CreateEmptyDatabase()
        {
            try
            {
                var  path = FileHelper.DatabaseSaveDialog();
                FileHelper.CopyFile(AppDomain.CurrentDomain.BaseDirectory + "/" + "testdatabase_v1.db", path);
                DataBaseFilePath = path;
                OKButtonHandler();
            }
            catch (Exception e)
            {
                _messageService.ShowError(e.Message);
            }
            
        }

        public void OnAppLoad()
        {
            ReadConfig();
        }
    }
}