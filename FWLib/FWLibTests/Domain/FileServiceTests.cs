﻿using System;
using System.IO;
using System.Reflection;
using FWLibUI.Domain.Abstract;
using FWLibUI.Domain.Concrete;
using FWLibUI.Domain.Services;
using NUnit.Framework;

namespace FWLibTests.Domain
{
    [TestFixture]
    public class FileServiceTests
    {
        private IFWFileRepository _fileRepository;
        private FWFileService _fileService;
        private AppConfigService _appConfigService;

        private readonly string _connectionString = Path.Combine(
            Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
            path2: @"testdatabase_v1.db");

        [SetUp]
        public void SetUp()
        {
            _appConfigService = new AppConfigService();
            var s = Path.Combine(
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                path2: AppConfigService.DefaultConfigFileName);
            
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)+ @"\";
            
            var config = _appConfigService.ReadAppConfig(path).Value;
            config.DataBaseFilePath = _connectionString;
            _appConfigService.SaveAppConfig(config, s);
        }

        [Test]
        public void GetAllFilesTest_ActualFileCount()
        {
            using (var context = EFDbContext.Create(_connectionString))
            {
                _fileRepository = new FWFileRepository(context);
                _fileService = new FWFileService();
                _fileService.FWFileServiceSetPepository(_fileRepository);

                var result = _fileService.GetAllFiles().Value;

                Assert.That(Is.Equals(result.Count, 4));
            }
        }

        [Test]
        public void GetAllFilesTest_ConcreteFileCount()
        {
            using (var context = EFDbContext.Create(_connectionString))
            {
                _fileRepository = new FWFileRepository(context);
                _fileService = new FWFileService();
                _fileService.FWFileServiceSetPepository(_fileRepository);

                var result = _fileService.GetAllFiles(0, 1).Value;

                Assert.That(Is.Equals(result.Count, 1));
            }
        }

        [Test]
        public void GetAllFileTypes_ActualFileTypes()
        {
            using (var context = EFDbContext.Create(_connectionString))
            {
                _fileRepository = new FWFileRepository(context);
                _fileService = new FWFileService();
                _fileService.FWFileServiceSetPepository(_fileRepository);

                var result = _fileService.GetAllFileTypes().Value;

                Assert.That(Is.Equals(result.Count, 2));
                Assert.That(Is.Equals(result[0], "Origin"));
            }
        }
    }
}