﻿using System;
using System.Data.SQLite;

namespace FWLibTests.Domain.TestFixture
{
    public class DataBaseEmptyFixture:IDisposable
    {
        public SQLiteConnection Connection { get; set; }

        public DataBaseEmptyFixture()
        {
            Connection = new SQLiteConnection("DataSource=:memory:");
            
            Connection.Open();
                       
        }

        public void Dispose()
        {
            Connection?.Close();
        }
    }
}