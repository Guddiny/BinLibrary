#tool "nuget:?package=NUnit.ConsoleRunner"

var target = Argument("target", "Default");

Task("Default")
  .IsDependentOn("Run");

Task("Nuget")
  .Does(() =>{
      NuGetRestore("FWLib.sln");
});

Task("Build")
  .IsDependentOn("Nuget")
  .Does(() =>{
  MSBuild("FWLib.sln", new MSBuildSettings {
    Configuration = "Release"
    });
});

Task("Tests")
  .IsDependentOn("Build")
  .Does(()=>{
    NUnit3("./FWLibTests/bin/Release/FWLibTests.dll");
  });


Task("Publish")
    .IsDependentOn("Tests")
    .Does(()=>{
        DeleteDirectory("../Publish", recursive:true);
        CreateDirectory("../Publish");
        CreateDirectory("../Publish/App");
        CopyDirectory("./FWLibUI/DeployContent", "../Publish");
        CopyDirectory("./FWLibUI/bin/Release", "../Publish/App");
        Information("Published!");
      });


Task("Run")
  .IsDependentOn("Publish")
  .Does(()=>{
    StartProcess("../Publish/App/FWLibUI.exe");
    Information("App started!");
  });





RunTarget(target);